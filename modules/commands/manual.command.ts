import { Command, CommandRunner } from "nest-commander";
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";


@Injectable()
@Command({
    name: 'manual',
    options: { isDefault: true }
})
export class ManualCommander implements CommandRunner {
    constructor(private config: ConfigService) { }

    async run(inputs: string[]): Promise<void> {

        /**
         * Runner can be:
         * yarn              for local run
         * docker-compose    for containerized version
         */
        const runner = this.config.get('MainConfig.cliEntry')

        console.log(`
    Hello From Concruise CLI!
    
    $ ${runner} run cli [command]

    -> $ customer   List all customers
    -> $ cruiser    List all cruisers
    -> $ match      Doing the match and show result, pass --rematch to force rematch
    -> $ seed       Seed the database from given file  
    -> $ manual     For help

    Example:
    ${runner} run cli manual

    `)

    }

}