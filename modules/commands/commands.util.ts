import { Connection } from 'mongoose';
import { Queue } from "bull"


/**
 * 
 * Polling the database until connection established
 * @param connection mongoose connection instance
 * @returns 
 */
export const waitForDatabase = async (connection: Connection): Promise<void> => {
    return new Promise((resolve, reject) => {

        var poll = setInterval(async () => {
            try {
                if (connection.readyState === 1) {
                    clearInterval(poll)
                    resolve(null)
                }

            } catch (e) {
            }

        }, 2000)
    })
}


/**
 * [-] Poll the queue and calculate the progress of the completed jobs
 * [-] Log the progress
 * [-] Resolves when all jobs are done
 * 
 * @param queue bull queue
 * @param totalSubmitedJobs total jobs sended to the queue, used to calculating the progress
 * @returns Prmoise
 */
export const waitForWorkers = async (queue: Queue, totalSubmitedJobs: number): Promise<void> => {
    return new Promise((resolve, reject) => {

        var poll = setInterval(async () => {
            try {

                const completedMatchesCount = await queue.getCompletedCount()
                const progress = Math.ceil((completedMatchesCount / totalSubmitedJobs) * 100)
                console.log(` -> Progress:  ${progress}%`)

                if (progress === 100) {
                    clearInterval(poll)
                    resolve(null)
                }

            } catch (e) {
            }

        }, 1000)
    })
}