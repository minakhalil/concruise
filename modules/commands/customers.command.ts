import { UserService } from "../user/user.service";
import { Command, CommandRunner } from "nest-commander";
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { Injectable } from "@nestjs/common";
import { UserType } from "../user/user.schema";
import { waitForDatabase } from "./commands.util";
import { filterAttrAndClean } from "../user/user.util";


@Command({
    name: 'customer'
})
@Injectable()
export class CustomerCommander implements CommandRunner {
    constructor(private readonly service: UserService, @InjectConnection() private connection: Connection) { }

    async run(inputs: string[]): Promise<void> {

        console.log(`
    Listing all customers
    `)

        await waitForDatabase(this.connection)

        const customers = await this.service.findAll({ type: UserType.CUSTOMER })

        console.table(filterAttrAndClean(customers))

    }
}