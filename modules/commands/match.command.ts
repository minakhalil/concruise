import { UserService } from "../user/user.service";
import { Command, CommandRunner, Option } from "nest-commander";
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { Injectable } from "@nestjs/common";
import { UserType } from "../user/user.schema";
import { MatchService } from "../match/match.service";
import { InjectQueue } from "@nestjs/bull";
import { Queue } from "bull"
import { waitForDatabase, waitForWorkers } from "./commands.util";
import { filterAttrAndClean as filterAttrAndCleanForUser } from "../user/user.util"
import { filterAttrAndClean as filterAttrAndCleanForMatch } from "../match/match.util"



/**
 * 
 *  Matching Command [CORE]
 * 
 *  [-] options
 *      --rematch   force doing rematch
 *      --nopoll    queue jobs then return without waiting for result
 */
@Command({
    name: 'match'
})
@Injectable()
export class MatchCommander implements CommandRunner {
    constructor(
        private readonly uservice: UserService,
        private readonly mservice: MatchService,
        @InjectConnection() private connection: Connection,
        @InjectQueue('match-requests') private matchRequestsQueue: Queue
    ) { }


    @Option({
        flags: '--rematch',
        description: 'Clear previous matches and redo it again',
    })
    parseRematch(val: string): string {
        return val;
    }


    @Option({
        flags: '--nopoll',
        description: "Don't wait for result",
    })
    parseNoPoll(val: string): string {
        return val;
    }

    async run(inputs: string[], options?: any): Promise<void> {

        await waitForDatabase(this.connection)

        const previousMatches = await this.mservice.getMatches()

        //If there is no previous matches or user force rematch
        if (previousMatches.length === 0 || options.rematch === true) {
            /**
             * 
             * Do Matching
             */

            if (previousMatches.length)
                console.log(`
    Clearing previous matches...
    `)

            // clear the current matches and empty the queue
            await Promise.all([
                this.uservice.unMatchAll(),
                this.mservice.dropMatches(),
                this.matchRequestsQueue.clean(0)
            ])


            console.log(`
    Matching
    Sending to workers...
    `)

            //pass every unmatched customer to the queue
            const unMatchedCustomers = await this.uservice.findAll({ type: UserType.CUSTOMER, matched: false })

            for (let customer of unMatchedCustomers)
                await this.matchRequestsQueue.add(customer)

            if (options.nopoll === true)
                return

            await waitForWorkers(this.matchRequestsQueue, unMatchedCustomers.length)

        } else
            console.log(`
    Fetching previous matches...
    `)



        /**
         * 
         * Show Matching Result
         */

        console.log(`
    List of matched customers
    `)
        const finalMatches = await this.mservice.getMatches()

        console.table(filterAttrAndCleanForMatch(finalMatches))


        console.log(`
    List of unmatched customers
    `)
        const unmatchedCustomers = await this.uservice.findAll({ type: UserType.CUSTOMER, matched: false })

        console.table(filterAttrAndCleanForUser(unmatchedCustomers))



        console.log(`
    List of unmatched cruisers
    `)
        const unmatchedCruisers = await this.uservice.findAll({ type: UserType.CRUISER, matched: false })

        console.table(filterAttrAndCleanForUser(unmatchedCruisers))

    }

}