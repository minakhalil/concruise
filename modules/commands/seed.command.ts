import { UserService } from "../user/user.service";
import { Command, CommandRunner, Option } from "nest-commander";
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { Injectable } from "@nestjs/common";
import { UserType } from "../user/user.schema";
import { waitForDatabase } from "./commands.util";
import * as XLSX from 'xlsx'
import { CreateUserDto } from "modules/user/user.dto";

@Command({
    name: 'seed'
})
@Injectable()
export class SeedCommander implements CommandRunner {
    constructor(
        private readonly uservice: UserService,
        @InjectConnection() private connection: Connection
    ) { }

    async run(inputs: string[]): Promise<void> {

        console.log(`
    Seeding Database...
    `)

        //read file sheets
        const workbook = XLSX.readFile("./data/Sample_Data.xlsx")
        const [cruisersSheet, customersSheet] = workbook.SheetNames

        //extract data from sheets
        const cruisers = XLSX.utils.sheet_to_json(workbook.Sheets[cruisersSheet])
        const customers = XLSX.utils.sheet_to_json(workbook.Sheets[customersSheet])

        //load all creation promises
        const creationPmoises = [
            ...cruisers.map((c: UserPayload) => this.writeUserToDB(c, UserType.CRUISER)),
            ...customers.map((c: UserPayload) => this.writeUserToDB(c, UserType.CUSTOMER))
        ]

        //execute all the promises in-parallel
        await waitForDatabase(this.connection)
        await Promise.all(creationPmoises)


        console.log(`
        Finish seeding.
        `)

    }

    async writeUserToDB(userPayload: UserPayload, type: UserType) {
        const { name, locationLatitude, locationLongitude, numberOfRides, rating } = userPayload

        const user: CreateUserDto = {
            fullName: name,
            rides: numberOfRides,
            rating,
            location: {
                coordinates: [locationLongitude, locationLatitude]
            },
            type
        }

        return this.uservice.create(user)

    }
}


type UserPayload = {
    name: string
    locationLatitude: number
    locationLongitude: number
    numberOfRides: number
    rating: number
}