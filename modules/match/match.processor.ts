import { Processor, Process } from '@nestjs/bull';
import { InjectConnection } from '@nestjs/mongoose';
import { Job } from 'bull';
import { MatchService } from './match.service';
import { UserService } from '../user/user.service';
import { Connection } from 'mongoose';
import { calculateScore } from './match.util';

@Processor('match-requests')
export class RequestsConsumer {
    constructor(
        private readonly uservice: UserService,
        private readonly mservice: MatchService,
        @InjectConnection() private connection: Connection,
    ) { }

    /**
     * 
     * Match Customer to Cruiser
     * 
     * [-] extract customer details from job payload
     * [-] get all nearby unmatched cruisers -sorted by distance-
     * [-] try to match one of them to the customer
     * [-] if match insertion not fails update both customer,cruiser to be matched=True
     * 
     * 
     * @param job bull job object {contains customer data}
     */
    @Process()
    async processRequest(job: Job<unknown>) {
        const customer = job.data as any

        const nearCruisers = await this.uservice.findNear(customer.location.coordinates)
        const matches = calculateScore(customer, nearCruisers)

        for (let match of matches) {
            try {
                await this.mservice.create(match)
                await Promise.all([
                    this.uservice.update(match.cruiser, { matched: true }),
                    this.uservice.update(match.customer, { matched: true })
                ])
                break
            } catch { }
        }

    }

}