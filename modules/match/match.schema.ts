import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';

export type MatchDocument = Match & Document;

@Schema()
export class Match {
    @Prop({ unique: true, sparse: true, type: MongooseSchema.Types.ObjectId, ref: 'User' })
    cruiser: Types.ObjectId


    @Prop({ unique: true, sparse: true, type: MongooseSchema.Types.ObjectId, ref: 'User' })
    customer: Types.ObjectId

    @Prop({ default: 0 })
    score?: Number;
}

const MatchSchema = SchemaFactory.createForClass(Match);



export {
    MatchSchema
}