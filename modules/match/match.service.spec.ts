import { Test, TestingModule } from '@nestjs/testing';
import { CreateMatchDto } from './match.dto';
import { MatchService } from './match.service';

describe('MatchService', () => {
  let service: MatchService;

  beforeEach(async () => {

    const MatchServiceProvider = {
      provide: MatchService,
      useFactory: () => ({
        create: jest.fn(() => []),
        dropMatches: jest.fn(() => { }),
        getMatches: jest.fn(() => { })
      })
    }

    const module: TestingModule = await Test.createTestingModule({
      providers: [MatchService, MatchServiceProvider],
    }).compile();

    service = module.get<MatchService>(MatchService);
  })


  it('should call create method with expected params', async () => {
    const createMatchSpy = jest.spyOn(service, 'create')
    const dto = new CreateMatchDto()
    service.create(dto)
    expect(createMatchSpy).toHaveBeenCalledWith(dto)
  })

});
