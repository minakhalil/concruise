import { Module } from '@nestjs/common';
import { MatchService } from './match.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MatchSchema, Match } from './match.schema';

@Module({
    providers: [MatchService],
    controllers: [],
    imports: [
        MongooseModule.forFeature([{ name: Match.name, schema: MatchSchema }]),
    ],
})
export class MatchModule { }
