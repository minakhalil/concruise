import { IsNotEmpty, IsString, Min, IsInt } from "class-validator"



export class CreateMatchDto {

    @IsNotEmpty()
    @IsString()
    customer: string

    @IsNotEmpty()
    @IsString()
    cruiser: string

    @IsNotEmpty()
    @IsInt()
    @Min(0)
    score: number

}
