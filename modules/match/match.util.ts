//used to foramt the ouput of MongooseDucment to be able to logged in control.table
export const filterAttrAndClean = (docs: any[]): any[] => {
    return docs.map(({
        _doc: {
            cruiser, customer, score
        }
    }) => Object({
        customerID: customer._id.toString(),
        customerFullName: customer.fullName,
        cruiserID: cruiser._id.toString(),
        cruiserFullName: cruiser.fullName,
        score
    }))
}


/**
 * 
 * Calculates the score based on the formula:
 * 
 *  7P -> Distance <= 3.0 km
 *  3P -> Distance <= 5.0 km
 *  2P -> Customer Rating >= Cruiser’s Rating (1 <= rating <= 5)
 *  5P -> Customer’s Rides <= 2 and Cruiser’s Rides >= 3
 *  2P -> Customer’s Rides > 2 and Cruiser’s Rides < 3
 * 
 * @param customer customer payload
 * @param cruisers list of nearby cruisers
 * @returns score
 */
export const calculateScore = (customer, cruisers) => {
    const matches = []


    for (let cruiser of cruisers) {

        let score = 0


        //Distance
        if (cruiser.distance <= 3000)
            score += 7
        else
            if (cruiser.distance <= 5000)
                score += 3

        //Rating
        if (customer.rating >= cruiser.rating)
            score += 2


        //Rides
        if (customer.rides <= 2 && cruiser.rides >= 3)
            score += 5
        else
            if (customer.rides > 2 && cruiser.rides < 3)
                score += 2

        matches.push({
            customer: customer._id,
            cruiser: cruiser._id,
            score
        })
    }

    matches.sort((a, b) => b.score - a.score)
    return matches
}