import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Match, MatchDocument } from './match.schema';

@Injectable()
export class MatchService {
    constructor(
        @InjectModel(Match.name) private readonly model: Model<MatchDocument>,
    ) { }

    async create(createPayload): Promise<any> {
        return await new this.model({
            ...createPayload
        }).save();
    }

    async dropMatches(): Promise<any> {
        await this.model.deleteMany({});
    }


    async getMatches(filter = {}): Promise<Match[]> {
        return await this.model.find(filter).sort( { score: -1 } ).populate('customer').populate('cruiser').exec();
    }
}
