import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { CreateUserDto, UpdateUserDto } from './user.dto';
import { UserService } from './user.service';

describe('UserController', () => {
  let controller: UserController;
  let spyService: UserService

  beforeEach(async () => {
    const UserServiceProvider = {
      provide: UserService,
      useFactory: () => ({
        findAll: jest.fn(() => []),
        findOne: jest.fn(() => { }),
        create: jest.fn(() => { }),
        update: jest.fn(() => { }),
        delete: jest.fn(() => { })
      })
    }

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService, UserServiceProvider]
    }).compile();

    controller = module.get<UserController>(UserController)
    spyService = module.get<UserService>(UserService)

  });

  it("calling create method", () => {
    const dto = new CreateUserDto()
    expect(controller.create(dto)).not.toEqual(null)
  })

  it("calling create method", () => {
    const dto = new CreateUserDto()
    controller.create(dto)
    expect(spyService.create).toHaveBeenCalled()
    expect(spyService.create).toHaveBeenCalledWith(dto)
  })


  it("calling findAll method", () => {
    controller.index();
    expect(spyService.findAll).toHaveBeenCalled();
  })

  it("calling findOne method", () => {
    const id = "555"
    controller.find(id);
    expect(spyService.findOne).toHaveBeenCalled();
  })


  it("calling delete method", () => {
    const id = "555"
    controller.delete(id);
    expect(spyService.delete).toHaveBeenCalled();
  })


  it("calling update method", () => {
    const id = "555"
    const dto = new UpdateUserDto()
    controller.update(id, dto);
    expect(spyService.update).toHaveBeenCalled();
  })

});
