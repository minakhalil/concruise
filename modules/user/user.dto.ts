import { IsNumber, IsObject, ValidateNested, IsNotEmpty, IsString, Min, Max, IsArray, ArrayMinSize, ArrayMaxSize, IsInt, IsOptional } from "class-validator"
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UserType } from "./user.schema";


export class LocationDto {
    readonly type?: string = 'Point'

    @IsNotEmpty()
    @IsNumber({}, { each: true })
    @IsArray()
    @ArrayMinSize(2)
    @ArrayMaxSize(2)
    @ApiProperty({ type: () => [Number, Number] })
    coordinates: number[]
}
export class CreateUserDto {

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    fullName: string

    @IsNotEmpty()
    @IsInt()
    @Min(0)
    @ApiProperty()
    rides: number

    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    @Max(5)
    @ApiProperty()
    rating: number


    @IsNotEmpty()
    @IsObject()
    @ValidateNested()
    @Type(() => LocationDto)
    @ApiProperty()
    location!: LocationDto

    type?: UserType
}


export class UpdateUserDto {

    @IsOptional()
    @IsString()
    @ApiProperty()
    fullName?: string


    @IsOptional()
    @IsInt()
    @Min(0)
    @ApiProperty()
    rides?: number

    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(5)
    @ApiProperty()
    rating?: number


    @IsOptional()
    @IsObject()
    @ValidateNested()
    @Type(() => LocationDto)
    @ApiProperty()
    location?: {
        type: string,
        coordinates: number[]
    }


    matched?: boolean
}