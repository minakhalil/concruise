import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto, UpdateUserDto } from './user.dto';
import { User, UserDocument, UserType } from './user.schema';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(User.name) private readonly model: Model<UserDocument>,
    ) { }



    async findAll(filter = {}): Promise<User[]> {
        return await this.model.find(filter).exec();
    }

    async findOne(id: string): Promise<User> {
        return await this.model.findById(id).exec();
    }

    async create(createPayload: CreateUserDto): Promise<User> {
        return await new this.model({
            ...createPayload
        }).save();
    }

    async update(id: string, updatePayload: UpdateUserDto): Promise<User> {
        return await this.model.findByIdAndUpdate(id, updatePayload, { new: true }).exec();
    }

    async unMatchAll(): Promise<any> {
        return await this.model.updateMany({}, { matched: false }).exec();
    }

    async delete(id: string): Promise<User> {
        return await this.model.findByIdAndDelete(id).exec();
    }


    /**
     * 
     * Find all nearby cruisers from the customer location
     * 
     * @param location [lng,lat]
     * @returns 
     */
    async findNear(location: any): Promise<any[]> {

        return await this.model
            .aggregate([
                {
                    $geoNear: {
                        near: {
                            type: "Point",
                            coordinates: location
                        },
                        query: {
                            matched: false,
                            type: UserType.CRUISER
                        },
                        key: "location",
                        distanceField: "distance",
                        spherical: true
                    }
                }
            ])
            .exec();
    }

}
