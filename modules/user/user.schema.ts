import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

export enum UserType {
    CUSTOMER = 'customer',
    CRUISER = 'cruiser'
}

@Schema()
export class UserLocation extends Document {
    @Prop({ required: true, default: "Point" })
    type: string;

    @Prop()
    coordinates: [Number];
}
export const LocationSchema = SchemaFactory.createForClass(UserLocation);


@Schema({ timestamps: true })
export class User {
    @Prop({ required: true })
    fullName: string;

    @Prop({ default: 4 })
    rating?: Number;

    @Prop({ default: 0 })
    rides?: Number;

    @Prop({ type: LocationSchema, _id: false })
    location: UserLocation;

    @Prop({ default: false })
    matched?: Boolean;

    @Prop({
        required: true,
        default: UserType.CUSTOMER
    })
    type: UserType;
}


const UserSchema = SchemaFactory.createForClass(User);

UserSchema.index({ location: '2dsphere' })


export {
    UserSchema
}