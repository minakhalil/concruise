import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserDto, UpdateUserDto } from './user.dto';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {

    const UserServiceProvider = {
      provide: UserService,
      useFactory: () => ({
        findAll: jest.fn(() => []),
        findOne: jest.fn(() => { }),
        create: jest.fn(() => { }),
        update: jest.fn(() => { }),
        delete: jest.fn(() => { })
      })
    }

    const module: TestingModule = await Test.createTestingModule({
      providers: [UserService, UserServiceProvider],
    }).compile();

    service = module.get<UserService>(UserService);
  })

  it('should call create method with expected params', async () => {
    const createUserSpy = jest.spyOn(service, 'create')
    const dto = new CreateUserDto()
    service.create(dto)
    expect(createUserSpy).toHaveBeenCalledWith(dto)
  })

  it('should call update method with expected params', async () => {
    const updateUserSpy = jest.spyOn(service, 'update');
    const id = '555';
    const dto = new UpdateUserDto();
    service.update(id, dto);
    expect(updateUserSpy).toHaveBeenCalledWith(id, dto);
  })

});
