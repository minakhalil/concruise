import { User } from "./user.schema"

//used to foramt the ouput of MongooseDucment to be able to logged in control.table
export const filterAttrAndClean = (docs: any[]): User[] => {
    return docs.map(({
        _doc: {
            _id, fullName, rating, rides, location: { coordinates }
        }
    }) => Object({
        id: _id.toString(),
        fullName,
        rides,
        rating,
        coordinates
    }))
}