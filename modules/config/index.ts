export default () => ({
    MainConfig: {
        hostedURL: process.env.HOST_URL || "localhost",
        port: parseInt(process.env.PORT) || 3200,
        serviceName: process.env.SRV_NAME || "concruise",
        logLevel: process.env.LOG_LVL || "debug",
        cliEntry: process.env.CLI_ENTRY || "yarn"
    },
    DatabaseConfig: {
        host: process.env.MONGO_SERVER || "127.0.0.1",
        port: parseInt(process.env.MONGO_PORT) || 27017,
        db: process.env.MONGO_DB_NAME || "concruise"
    },
    RedisConfig: {
        host: process.env.REDIS_SERVER || "127.0.0.1",
        port: parseInt(process.env.REDIS_PORT) || 6379,
        queueName: 'match-requests'
    }
})