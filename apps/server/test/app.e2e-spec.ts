import { Test, TestingModule } from '@nestjs/testing';
import { BadRequestException, INestApplication, ValidationError, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { closeInMongodConnection, rootMongooseTestModule } from '../../../modules/test/test.util';
import { UserModule } from '../../../modules/user/user.module';
import { User, UserSchema, UserType } from '../../../modules/user/user.schema';
import { CreateUserDto } from 'modules/user/user.dto';

describe('UsersController (e2e)', () => {
  let app: INestApplication;
  let connection

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        UserModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication()
    app.useGlobalPipes(new ValidationPipe({
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        return new BadRequestException(validationErrors[0]);
      },
    }))

    connection = await moduleFixture.get(getConnectionToken());

    await app.init();
  });

  it('should create new customer with same submitted data', async () => {
    const user: CreateUserDto = {
      fullName: "test",
      rides: 5,
      rating: 5,
      location: {
        coordinates: [-1, 2]
      }
    }
    await request(app.getHttpServer())
      .post('/user')
      .send(user)
      .expect(201)
      .expect(res => {
        const { fullName, rides, rating, location, type } = res.body
        expect(fullName).toEqual(user.fullName)
        expect(rides).toEqual(user.rides)
        expect(rating).toEqual(user.rating)
        expect(type).toEqual(UserType.CUSTOMER)
        expect(type).toEqual(UserType.CUSTOMER)
        expect(location.coordinates[0]).toEqual(user.location.coordinates[0])
        expect(location.coordinates[1]).toEqual(user.location.coordinates[1])
      })
  })



  it('should refuse to create customer if missing name,rating,location', async () => {
    await Promise.all([
      request(app.getHttpServer())
        .post('/user')
        .send({
          fullName: "test"
        })
        .expect(400)
      ,


      request(app.getHttpServer())
        .post('/user')
        .send({
          fullName: "test",
          rides: 5
        })
        .expect(400)
      ,

      request(app.getHttpServer())
        .post('/user')
        .send({
          rating: 4
        })
        .expect(400)
      ,

      request(app.getHttpServer())
        .post('/user')
        .send({
          fullName: "test",
          rides: 5,
          rating: 5
        })
        .expect(400),
    ])
  })





  it('should refuse to create customer if wrong rating range or location format', async () => {
    await Promise.all([
      request(app.getHttpServer())
        .post('/user')
        .send({
          fullName: "test",
          rides: 5,
          rating: 7
        })
        .expect(400)
      ,


      request(app.getHttpServer())
        .post('/user')
        .send({
          fullName: "test",
          rides: 5,
          rating: 5,
          location: 500
        })
        .expect(400),


      request(app.getHttpServer())
        .post('/user')
        .send({
          fullName: "test",
          rides: 5,
          rating: 5,
          location: [500]
        })
        .expect(400)
    ])
  })



  it('should update any data for the inserted customer', async () => {

    let user: CreateUserDto = {
      fullName: "test",
      rides: 5,
      rating: 5,
      location: {
        coordinates: [-1, 2]
      }
    }

    const { body: savedUser } = await request(app.getHttpServer())
      .post('/user')
      .send(user)

    const newName = "updated test"

    await request(app.getHttpServer())
      .put(`/user/${(savedUser as any)._id.toString()}`)
      .send({
        fullName: newName
      })
      .expect(200)
      .expect(res => expect(res.body.fullName).toEqual(newName))


    await request(app.getHttpServer())
      .get(`/user/${(savedUser as any)._id.toString()}`)
      .expect(200)
      .expect(res => expect(res.body.fullName).toEqual(newName))

  })

  afterEach(async () => {
    await connection.close()
    await closeInMongodConnection()
  })

})
