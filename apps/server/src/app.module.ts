import { Module } from '@nestjs/common';
import { UserModule } from '../../../modules/user/user.module';

import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import config from '../../../modules/config';

@Module({
  imports: [
    
    ConfigModule.forRoot({
      load: [config]
    }),


    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: `mongodb://${configService.get<string>('DatabaseConfig.host')}/${configService.get<string>('DatabaseConfig.db')}`,
      }),
      inject: [ConfigService]
    }),


    UserModule
  ]
})
export class AppModule { }
