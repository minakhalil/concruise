import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { UserModule } from '../../../modules/user/user.module';
import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import * as fs from 'fs'

if (fs.existsSync(`./local.env`))
     require('dotenv').config({ path: `./local.env` })


async function bootstrap() {


  /**
   * Init Api
   */

  const app = await NestFactory.create(AppModule)
  const configService: ConfigService = app.get<ConfigService>(ConfigService);


  /**
   * Attach Prefix, Validation Middleware
   */
  app.setGlobalPrefix('api')
  app.useGlobalPipes(new ValidationPipe({
    exceptionFactory: (validationErrors: ValidationError[] = []) => {
      return new BadRequestException(validationErrors[0]);
    },
  }));


  /**
   * Swagger Setup
   */
  const config = new DocumentBuilder()
    .setTitle(configService.get('MainConfig.serviceName'))
    .setDescription('Api for boat ride-sharing platform')
    .setVersion('1.0')
    .build()
  const document = SwaggerModule.createDocument(app, config, {
    include: [UserModule],
  })

  SwaggerModule.setup('docs', app, document, {
    explorer: true,
    customCss: '.swagger-ui .topbar { display: none }',
    customSiteTitle: `${configService.get('MainConfig.serviceName')} Api Documentation`
  })


  /**
   * Run Server
   */
  await app.listen(configService.get('MainConfig.port'));
}
bootstrap();
