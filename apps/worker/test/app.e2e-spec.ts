import { Test, TestingModule } from '@nestjs/testing';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';

import { rootMongooseTestModule, closeInMongodConnection } from '../../../modules/test/test.util';
import { User, UserSchema } from '../../../modules/user/user.schema';
import { Match, MatchSchema } from '../../../modules/match/match.schema';
import { UserService } from '../../../modules/user/user.service';
import { RequestsConsumer } from '../../../modules/match/match.processor';
import { MatchService } from '../../../modules/match/match.service';
import { UserType } from '../../../modules/user/user.schema';
import { getQueueToken } from '@nestjs/bull';



describe('WorkerController (e2e)', () => {
  let moduleFixture: TestingModule
  let uService
  let mService
  let requestProcessor
  let connection

  const mockQueue = {
    close: jest.fn()
  }

  beforeEach(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        MongooseModule.forFeature([{ name: Match.name, schema: MatchSchema }])
      ],
      providers: [
        UserService,
        MatchService,
        RequestsConsumer
      ]
    })
      .overrideProvider(getQueueToken('match-requests'))
      .useValue(mockQueue).compile()

    uService = await moduleFixture.get<UserService>(UserService)
    mService = await moduleFixture.get<MatchService>(MatchService)
    requestProcessor = await moduleFixture.get<RequestsConsumer>(RequestsConsumer)
    connection = await moduleFixture.get(getConnectionToken())
  })

  it('should match customer with cruiser', async () => {

    const [customer, cruiser] = await Promise.all([
      uService.create({
        fullName: "customerName",
        rating: 4,
        rides: 5,
        location: {
          type: "Point",
          coordinates: [-1, -2]
        },
        type: UserType.CUSTOMER
      }),

      uService.create({
        fullName: "cruiserName",
        rating: 4,
        rides: 5,
        location: {
          type: "Point",
          coordinates: [-1, -2]
        },
        type: UserType.CRUISER
      })
    ])    

    await requestProcessor.processRequest({
      data: customer
    })

    const [customerAfterMatch, cruiserAfterMatch, [match]] = await Promise.all([
      uService.findOne(customer._id),
      uService.findOne(cruiser._id),
      mService.getMatches({ customer: customer._id })
    ])

    expect(customerAfterMatch.matched).toEqual(true)
    expect(cruiserAfterMatch.matched).toEqual(true)
    expect(match.cruiser._id.toString()).toEqual(cruiser._id.toString())
    expect(match.score).toEqual(9) // 7xdistance + 2xrating
  })



  it('should match customer with nearest cruiser', async () => {

    const [customer, farCruiser, nearCruiser] = await Promise.all([
      uService.create({
        fullName: "customerName",
        rating: 4,
        rides: 5,
        location: {
          type: "Point",
          coordinates: [-1, -2]
        },
        type: UserType.CUSTOMER
      }),

      uService.create({
        fullName: "cruiserName2",
        rating: 4,
        rides: 5,
        location: {
          type: "Point",
          coordinates: [-10, -20]
        },
        type: UserType.CRUISER
      }),

      uService.create({
        fullName: "cruiserName1",
        rating: 4,
        rides: 5,
        location: {
          type: "Point",
          coordinates: [-1, -2]
        },
        type: UserType.CRUISER
      }),



    ])


    await requestProcessor.processRequest({
      data: customer
    })

    const [customerAfterMatch, nearCruiserAfterMatch, farCruiserAfterMatch, [match]] = await Promise.all([
      uService.findOne(customer._id),
      uService.findOne(nearCruiser._id),
      uService.findOne(farCruiser._id),
      mService.getMatches({ customer: customer._id })
    ])

    expect(customerAfterMatch.matched).toEqual(true)
    expect(nearCruiserAfterMatch.matched).toEqual(true)
    expect(farCruiserAfterMatch.matched).toEqual(false)
    expect(match.cruiser._id.toString()).toEqual(nearCruiser._id.toString())
    expect(match.score).toEqual(9) // 7xdistance + 2xrating
  })


  afterEach(async () => {
    await moduleFixture.close()
    await mockQueue.close()
    await closeInMongodConnection()
  })

})
