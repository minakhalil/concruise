import { NestFactory } from '@nestjs/core';
import { WorkerModule } from './worker.module';
import * as fs from 'fs'

if (fs.existsSync(`./local.env`))
     require('dotenv').config({ path: `./local.env` })

     
async function bootstrap() {
  const app = await NestFactory.createApplicationContext(WorkerModule);
}
bootstrap();
