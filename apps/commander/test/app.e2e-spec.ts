import { TestingModule } from '@nestjs/testing';
import { CommanderModule } from './../src/commander.module';
import { closeInMongodConnection, rootMongooseTestModule } from '../../../modules/test/test.util';
import { CommandTestFactory } from 'nest-commander-testing'
import { BullModule, getQueueToken } from '@nestjs/bull';
import { UserService } from '../../../modules/user/user.service';
import { User, UserSchema } from '../../../modules/user/user.schema';
import { UserType } from '../../../modules/user/user.schema';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { MatchCommander } from '../../../modules/commands/match.command';
import { Match,MatchSchema } from '../../../modules/match/match.schema';
import { MatchService } from '../../../modules/match/match.service';


describe('CommanderController (e2e)', () => {
  let commandInstance: TestingModule;
  let uService
  let connection

  const mockQueue = {
    add: jest.fn(),
    clean: jest.fn()
  }

  beforeEach(async () => {
    commandInstance = await CommandTestFactory.createTestingCommand({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        MongooseModule.forFeature([{ name: Match.name, schema: MatchSchema }]),
        BullModule.registerQueue({
          name: 'match-requests',
        }),
      ],
      providers: [
        UserService,
        MatchService,
        MatchCommander
      ]
    })
      .overrideProvider(getQueueToken('match-requests'))
      .useValue(mockQueue)
      .compile()

    connection = await commandInstance.get(getConnectionToken())
    uService = await commandInstance.get<UserService>(UserService)
  })

  it('It should add messages to queue if there CUSTOMER not matched', async () => {

    await uService.create({
      fullName: "customerName",
      rating: 4,
      rides: 5,
      type: UserType.CUSTOMER
    })

    await CommandTestFactory.run(commandInstance, [
      'match',
      '--rematch',
      '--nopoll'
    ])

    expect(mockQueue.add).toHaveBeenCalled()

  });


  afterEach(async () => {
    await closeInMongodConnection()
  })
});
