import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../../../modules/user/user.schema';
import { UserService } from '../../../modules/user/user.service';
import config from '../../../modules/config';
import { Match, MatchSchema } from '../../../modules/match/match.schema';
import { MatchService } from '../../../modules/match/match.service';
import { BullModule } from '@nestjs/bull';

import { ManualCommander } from '../../../modules/commands/manual.command';
import { CruiserCommander } from '../../../modules/commands/cruiser.command';
import { CustomerCommander } from '../../../modules/commands/customers.command';
import { MatchCommander } from '../../../modules/commands/match.command';
import { SeedCommander } from '../../../modules/commands/seed.command';


@Module({
  imports: [

    ConfigModule.forRoot({
      load: [config]
    }),


    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: `mongodb://${configService.get<string>('DatabaseConfig.host')}/${configService.get<string>('DatabaseConfig.db')}`,
      }),
      inject: [ConfigService]
    }),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([{ name: Match.name, schema: MatchSchema }]),

    
    BullModule.registerQueueAsync({
      imports: [ConfigModule],
      name: 'match-requests',
      useFactory: async (configService: ConfigService) => ({
        redis: {
          host: configService.get<string>('RedisConfig.host'),
          port: configService.get<number>('RedisConfig.port')
        },
        prefix: configService.get<string>('MainConfig.serviceName')
      }),
      inject: [ConfigService],
    }),
  ],

  providers: [
    UserService,
    MatchService,


    ManualCommander,
    CruiserCommander,
    CustomerCommander,
    MatchCommander,
    SeedCommander

  ],
})
export class CommanderModule { }
