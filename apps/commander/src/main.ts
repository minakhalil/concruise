import { CommandFactory } from 'nest-commander';
import { CommanderModule } from './commander.module';
import * as fs from 'fs'

if (fs.existsSync(`./local.env`))
     require('dotenv').config({ path: `./local.env` })


async function bootstrap() {

     await CommandFactory.run(CommanderModule, ['error'])

}
bootstrap();
