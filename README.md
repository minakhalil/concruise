# Concruise
Platform to provide matching between customers and cruisers.


## Architecture

The Solution consists of 3 apps:

**Server** A rest api for customers.

**Commander** A commandline interface for team to do matching.

**Worker(s)** For the matching job.


When the operations team starts a match, the cli app gets all unmatched customers and starts to queue up a matching job for him.

When worker(s) become active, they start consuming the match jobs. for each one, they fetch all the near unmatched cruisers and rank them, then try to do a match until success.

While workers are doing the job, the cli app periodically polls the broker to estimate the progress of the matching operation until it finishes.

## Installation

Build/Start the databases and main services
```
$ docker-compose up -d mongo redis worker api
```

*If you need to scale workers for faster matching*
```
$ docker-compose up -d mongo redis worker api --scale worker=3
```

For the cli commands,
 
*First run may take some time until build the image*
```
$ docker-compose run cli [COMMAND]
```


Available commands 
```
customer    List all customers
cruiser     List all cruisers
match       Doing the match and show result, pass --rematch to force rematch
seed        Seed the database from given file  
manual      For help

For example:
$ docker-compose run cli manual
```

To seed the database 
```
$ docker-compose run cli seed
```

The main api listens on port `3200` and prefix `/api`
```
http://localhost:3200/api
```

## Local Build, Tests and Run 
Using yarn
*Edit local.env with your databases configration before run*
```
$ yarn --ignore-engines

$ yarn build commander

$ yarn test
$ yarn test:e2e:server
$ yarn test:e2e:commander
$ yarn test:e2e:worker

$ yarn start server
$ yarn start worker
$ yarn run cli [COMMAND]
```



## Docs

Swagger is available for API on `/docs`
```
http://localhost:3200/docs
```
